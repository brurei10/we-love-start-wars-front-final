export interface CategoriaDTO {
    id: string;
    nome: string;
    img: string;
}
